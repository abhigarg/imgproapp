package com.abggcv.imgproapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.view.menu.ActionMenuItemView;
import android.support.v7.widget.ActionMenuView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.edmodo.cropper.cropwindow.edge.Edge;
import com.hanks.htextview.HTextView;
import com.hanks.htextview.HTextViewType;
import com.squareup.picasso.Picasso;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.InjectView;
import uk.co.senab.photoview.PhotoViewAttacher;

public class MainActivity extends MainActivityCrop {
    private String TAG = "MainActivity";
    @InjectView(R.id.drawView)
    DrawingView drawView;
    @InjectView(R.id.title)
    HTextView title;
    @InjectView(R.id.image_open_layout)
    LinearLayout image_open_layout;
    @InjectView(R.id.crop_layout)
    LinearLayout crop_layout;

    private boolean isFirstIterationCompleted;
    private Mat inputImage, croppedImage, mask, maskOut, resultImage;
    private Point minXY = new Point();
    private Point maxXY = new Point();
    private Bitmap originalBitmap = null;
    private Bitmap croppedImageBitmap = null;
    private ImageView imageResult;
    public PhotoViewAttacher mAttacher;
    private List<Point> ptsBack, ptsItem, ptsEnd;
    private float fx, fy;
    private int bgd = 0, fgd = 1, prfgd = 3, prbgd = 2;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        title.setAnimateType(HTextViewType.ANVIL);
        title.animateText(MainActivity.this.getResources().getString(R.string.app_name));
        setupEvenlyDistributedToolbar();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1 && resultCode == RESULT_OK && null != data) {
            image_open_layout.setVisibility(View.GONE);
            crop_layout.setVisibility(View.VISIBLE);

            Uri imageFileUri = data.getData();
            Picasso.with(this).load(imageFileUri).into(cropImageView);
            mAttacher = new PhotoViewAttacher(cropImageView);
            drawView.clearAll();
            isFirstIterationCompleted = false;
            resultImage = new Mat();
            inputImage = new Mat();
            mask = new Mat();
            maskOut = new Mat();
            croppedImage = new Mat();

            ptsBack = new ArrayList<>();
            ptsItem = new ArrayList<>();
            ptsEnd = new ArrayList<>();
        }
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.bottom_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_choose_picture:
                Intent choosePictureIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(choosePictureIntent, 1);
                break;
            case R.id.action_crop_image:
                mAttacher.setZoomable(false);
                cropImageView.enableCrop();
                drawView.setType(DrawingView.TYPE.END);
                //mAttacher.update();
                //final Bitmap croppedImage = cropImageView.getCroppedImage();
                //cropImageView.setImageBitmap(croppedImage);
                break;
            case R.id.action_select_crop_item:
                //cropImageView.disableCrop();
                mAttacher.setZoomable(false);
                drawView.setType(DrawingView.TYPE.ITEM);
                break;
            case R.id.action_select_bk:
                //cropImageView.disableCrop();
                mAttacher.setZoomable(false);
                drawView.setType(DrawingView.TYPE.BACKGROUND);
                break;
            case R.id.action_apply:
                applyCrop();
                cropImageView.disableCrop();
                mAttacher.setZoomable(true);
                break;
        }
        return true;
    }

    public void applyCrop() {
        if(!isFirstIterationCompleted) {
            imageResult = cropImageView;

            croppedImageBitmap = cropImageView.getCroppedImage();

            Utils.bitmapToMat(croppedImageBitmap, croppedImage);

            Drawable drawable = cropImageView.getDrawable();

            android.graphics.Rect rect = drawable.getBounds();

            Log.d(TAG, "rect left: " + rect.left + ", rect top: " + rect.top + ", rect right: " + rect.right + ", rect bottom: " + rect.bottom);

            if (drawable != null && drawable instanceof BitmapDrawable) {
                originalBitmap = ((BitmapDrawable) drawable).getBitmap();
            }

            if (originalBitmap == null) {
                Toast.makeText(this, "Bitmap NULL", Toast.LENGTH_SHORT).show();
                return;
            }

            Utils.bitmapToMat(originalBitmap, inputImage);

            Log.d(TAG, "Size of image (numRows, numCols) " + inputImage.rows() + "x" + inputImage.cols());

            fx = (float) originalBitmap.getWidth() / (float) drawView.getWidth();
            fy = (float) originalBitmap.getHeight() / (float) drawView.getHeight();

            //float gx = (float) originalBitmap.getWidth() / (float) drawView.getWidth();
            //float gy = (float) originalBitmap.getHeight() / (float) drawView.getHeight();

            Log.e("FACTOR ", " fx = " + fx + " fy " + fy);

            getMaskPoints(drawView.getPath(DrawingView.TYPE.ITEM), ptsItem);
            getMaskPoints(drawView.getPath(DrawingView.TYPE.BACKGROUND), ptsBack);

            for (Point p : ptsBack) {
                Log.e("ptsBack", " X " + p.x + " Y " + p.y);
            }

            for (Point p : ptsItem) {
                Log.e("ptsItem", " X " + p.x + " Y " + p.y);
            }

            minXY.x = Edge.LEFT.getCoordinate() * fx/5; //drawView.minXY.x * fx;  //left top - x
            minXY.y = Edge.TOP.getCoordinate() * fy/5;  // left top - y

            maxXY.x = Edge.RIGHT.getCoordinate() * fx/5; // right bottom - x
            maxXY.y = Edge.BOTTOM.getCoordinate() * fy/5; // right bottom - y

            Log.e("PRINT", "Min Max mx" + minXY.x + " my " + minXY.y + " Mx " + maxXY.x + " My " + maxXY.y);


            if (ptsItem.isEmpty())
                Toast.makeText(this, "No object/background points selected. Please touch to select some object points.",
                        Toast.LENGTH_SHORT).show();
            else {
                Log.d(TAG, "Number of foreground points: " + ptsItem.size());
                Log.d(TAG, "Size of bitmap (Height, Width): " + originalBitmap.getHeight() + "x" + originalBitmap.getWidth());

                mask = new Mat((int) (croppedImage.rows() / 5), (int) (croppedImage.cols() / 5), CvType.CV_8UC1, new Scalar(prbgd)); //set to BGD

                Log.d(TAG, "Size of mask(numRows, numCols): " + mask.rows() + "x" + mask.cols());

                //set rectangle to Probable_Foreground
                //Core.rectangle(mask, minXY, maxXY, new Scalar(3), -1);
                //Rect maskRect = new Rect(minXY, maxXY);
                //mask.submat(maskRect).setTo(new Scalar(Imgproc.GC_PR_BGD));

                for (int i = 0; i < ptsItem.size(); i++) { //sure foreground
                    //mask.put((int)( ptsItem.get(i).y - minXY.y), (int) (ptsItem.get(i).x - minXY.x), (byte) (fgd & 0xFF));
                    Core.circle(mask, new Point(ptsItem.get(i).x - minXY.x, ptsItem.get(i).y - minXY.y), 2, new Scalar(fgd));
                }

                ptsItem.clear();

                for (int i = 0; i < ptsBack.size(); i++) { //sure background
                    //mask.put((int) (ptsBack.get(i).y - minXY.y), (int) (ptsBack.get(i).x - minXY.x), (byte) (bgd & 0xFF));
                    Core.circle(mask, new Point(ptsBack.get(i).x - minXY.x, ptsBack.get(i).y - minXY.y), 2, new Scalar(bgd));
                }

                ptsBack.clear();

                String extStorageDirectory = Environment.getExternalStorageDirectory().getPath();

                String imgName = extStorageDirectory + "/input.jpg";
                String outImgName = extStorageDirectory + "/output.jpg";
                Log.d(TAG, "output path: " + outImgName);

                ApplyGrabcut(croppedImage.getNativeObjAddr(), mask.getNativeObjAddr(), maskOut.getNativeObjAddr());

                isFirstIterationCompleted = true;
                drawView.clearAll();
                Toast.makeText(this, "Filter applied", Toast.LENGTH_SHORT).show();

                //Apply filter to input image as per output mask
                ApplyFilter(inputImage.getNativeObjAddr(), maskOut.getNativeObjAddr(), resultImage.getNativeObjAddr(),
                        (int) minXY.x, (int) minXY.y, (int) maxXY.x, (int) maxXY.y);

                //Picasso.with(this).lo
                Utils.matToBitmap(resultImage, originalBitmap);
                imageResult.setImageBitmap(originalBitmap);
                mAttacher.update();
            }
        }
        else {
            getMaskPoints(drawView.getPath(DrawingView.TYPE.ITEM), ptsItem);
            getMaskPoints(drawView.getPath(DrawingView.TYPE.BACKGROUND), ptsBack);
            //getMaskPoints(drawView.getPath(DrawingView.TYPE.END), ptsEnd);

            if (!ptsItem.isEmpty() || !ptsBack.isEmpty()) {
                if (!ptsItem.isEmpty()) {
                    for (int i = 0; i < ptsItem.size(); i++) { //sure foreground
                        //mask.put((int)( ptsItem.get(i).y - minXY.y), (int) (ptsItem.get(i).x - minXY.x), (byte) (fgd & 0xFF));
                        Core.circle(mask, new Point(ptsItem.get(i).x - minXY.x, ptsItem.get(i).y - minXY.y), 2, new Scalar(fgd));
                    }

                    ptsItem.clear();
                }

                if (ptsBack.isEmpty()) {
                    for (int i = 0; i < ptsBack.size(); i++) { //sure background
                        //mask.put((int) (ptsBack.get(i).y - minXY.y), (int) (ptsBack.get(i).x - minXY.x), (byte) (bgd & 0xFF));
                        Core.circle(mask, new Point(ptsBack.get(i).x - minXY.x, ptsBack.get(i).y - minXY.y), 2, new Scalar(bgd));
                    }

                    ptsBack.clear();
                }

                ApplyGrabcut(croppedImage.getNativeObjAddr(), mask.getNativeObjAddr(), maskOut.getNativeObjAddr());

                //isFirstIterationCompleted = true;
                drawView.clearAll();

                Toast.makeText(this, "Filter applied", Toast.LENGTH_SHORT).show();

                //Apply filter to input image as per output mask
                ApplyFilter(inputImage.getNativeObjAddr(), maskOut.getNativeObjAddr(), resultImage.getNativeObjAddr(), (int) minXY.x,
                        (int) minXY.y, (int) maxXY.x, (int) maxXY.y );

                //Picasso.with(this).lo
                Utils.matToBitmap(resultImage, originalBitmap);
                imageResult.setImageBitmap(originalBitmap);
                mAttacher.update();
            }
            else {
                ApplyGrabcut(croppedImage.getNativeObjAddr(), mask.getNativeObjAddr(), maskOut.getNativeObjAddr());

                //isFirstIterationCompleted = true;
                drawView.clearAll();

                Toast.makeText(this, "Filter applied", Toast.LENGTH_SHORT).show();

                //Apply filter to input image as per output mask
                ApplyFilter(inputImage.getNativeObjAddr(), maskOut.getNativeObjAddr(), resultImage.getNativeObjAddr() , (int) minXY.x,
                        (int) minXY.y, (int) maxXY.x, (int) maxXY.y);

                //Picasso.with(this).lo
                Utils.matToBitmap(resultImage, originalBitmap);
                imageResult.setImageBitmap(originalBitmap);
                mAttacher.update();
                //originalBitmap.

                //Toast.makeText(this, "Choose either item or background points", Toast.LENGTH_LONG).show();
            }
        }

    }


    public void getMaskPoints(Path fromPath, List<Point> toMask) {
        PathMeasure pm = new PathMeasure(fromPath, false);
        float length = pm.getLength();
        float distance = 0f;
        float speed = length / 20;
        int counter = 0;
        float[] aCoordinates = new float[2];

        while ((distance < length) && (counter < 20)) {
            // get point from the path
            pm.getPosTan(distance, aCoordinates, null);

            // Scale
            aCoordinates[0] *= fx/5;
            aCoordinates[1] *= fy/5;

            toMask.add(new Point(aCoordinates[0], aCoordinates[1]));
            counter++;
            distance = distance + speed;
        }
    }


    public void myOnClick(View view) {
        Log.e("=========", "click");

        switch (view.getId()) {
            case R.id.choose_image:
                Intent choosePictureIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(choosePictureIntent, 1);
                break;
            case R.id.btn_crop:
                final Bitmap croppedImage = cropImageView.getCroppedImage();
                cropImageView.setImageBitmap(croppedImage);
                break;
            case R.id.btn_step1:
                /*Log.i(TAG, "crop x1: " + cropImageView.getClipBounds().left + ", crop y1: " + cropImageView.getClipBounds().top +
                                "crop x2: " + cropImageView.getClipBounds().right + "crop y2: " + cropImageView.getClipBounds().bottom);
                */
                drawView.setType(DrawingView.TYPE.ITEM);
                //viewFlipper.showNext();
                break;
            case R.id.btn_step2:
                drawView.setType(DrawingView.TYPE.BACKGROUND);
                //viewFlipper.showNext();
                break;
            case R.id.filter:
                applyCrop();
        }
    }



    /**
     * This method will take however many items you have in your
     * menu/menu_main.xml and distribute them across your devices screen
     * evenly using a Toolbar. Enjoy!!
     */
    public void setupEvenlyDistributedToolbar(){
        // Use Display metrics to get Screen Dimensions
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);

        // Toolbar
        //mToolbar = (Toolbar) findViewById(R.id.toolbar_bottom);
        // Inflate your menu
        mToolbar.inflateMenu(R.menu.bottom_menu);

        // Add 10 spacing on either side of the toolbar
        mToolbar.setContentInsetsAbsolute(10, 10);

        // Get the ChildCount of your Toolbar, this should only be 1
        int childCount = mToolbar.getChildCount();
        // Get the Screen Width in pixels
        int screenWidth = metrics.widthPixels;

        // Create the Toolbar Params based on the screenWidth
        Toolbar.LayoutParams toolbarParams = new Toolbar.LayoutParams(screenWidth, Toolbar.LayoutParams.WRAP_CONTENT);

        // Loop through the child Items
        for(int i = 0; i < childCount; i++){
            // Get the item at the current index
            View childView = mToolbar.getChildAt(i);
            // If its a ViewGroup
            if(childView instanceof ViewGroup){
                // Set its layout params
                childView.setLayoutParams(toolbarParams);
                // Get the child count of this view group, and compute the item widths based on this count & screen size
                int innerChildCount = ((ViewGroup) childView).getChildCount();
                int itemWidth  = (screenWidth / innerChildCount);
                // Create layout params for the ActionMenuView
                ActionMenuView.LayoutParams params = new ActionMenuView.LayoutParams(itemWidth, Toolbar.LayoutParams.WRAP_CONTENT);
                // Loop through the children
                for(int j = 0; j < innerChildCount; j++){
                    View grandChild = ((ViewGroup) childView).getChildAt(j);
                    if(grandChild instanceof ActionMenuItemView){
                        // set the layout parameters on each View
                        grandChild.setLayoutParams(params);
                    }
                }
            }
        }
    }


    @SuppressWarnings("JniMissingFunction")
    public native int ApplyGrabcut(long imgAdd, long maskAdd, long maskOutAdd); //int minX, int minY, int maxX, int maxY, int runType);

    @SuppressWarnings("JniMissingFunction")
    public native int ApplyFilter(long imgAdd, long maskAdd, long resultAdd, int minX, int minY, int maxX, int maxY);
}
