package com.abggcv.imgproapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.edmodo.cropper.cropwindow.edge.Edge;

import org.opencv.core.Point;

/**
 * Created by shakti on 1/3/2016.
 */
class DrawingView extends View {
    public enum TYPE {ITEM, BACKGROUND, END}


    public int[] colors = {Color.GREEN, Color.RED, Color.BLUE};

    Paint mPaint;
    //MaskFilter  mEmboss;
    //MaskFilter  mBlur;
    Bitmap mBitmap;
    Canvas mCanvas;
    Path mPath;
    Paint mBitmapPaint;
    Path paths[] = new Path[TYPE.END.ordinal()+1];
    public Point minXY = new Point();
    public Point maxXY = new Point();

    Paint paints[] = new Paint[TYPE.END.ordinal()];
    TYPE mType = TYPE.END;


    public DrawingView(Context context) {
        super(context);
        init();
    }

    public DrawingView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    private void init() {
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        //mPaint.setColor(0xFFFF0000);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(5);

        mBitmapPaint = new Paint();
        mBitmapPaint.setColor(Color.RED);

        for (int i = 0; i < TYPE.END.ordinal(); i++) {
            paths[i] = new Path();
        }

        setupType();
    }

    public void setType(TYPE type) {
        mType = type;
        setupType();
    }

    private void setupType() {
        int typeIndex = mType.ordinal();
        mPath = paths[typeIndex];
        mPaint.setColor(colors[mType.ordinal()]);
    }

    public Path getPath(TYPE type) {
        return paths[type.ordinal()];
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if (w <= 0 || h <= 0)
            return;
        mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(mBitmap);

        minXY.x = mBitmap.getWidth();
        minXY.y = mBitmap.getHeight();
        maxXY.x = 0;
        maxXY.y = 0;
    }

    @Override
    public void draw(Canvas canvas) {
        // TODO Auto-generated method stub
        super.draw(canvas);
        canvas.drawBitmap(mBitmap, 0, 0, mBitmapPaint);
        //canvas.drawPath(mPath, mPaint);

        for (int i = 0; i < TYPE.END.ordinal(); i++) {
            mPaint.setColor(colors[i]);
            canvas.drawPath(paths[i], mPaint);
        }
    }

    private float mX, mY;
    private static final float TOUCH_TOLERANCE = 4;

    private void touch_start(float x, float y) {
        //mPath.reset();
        mPath.moveTo(x, y);
        mX = x;
        mY = y;
        //recordMinMax();
        //Log.e("DrawingView", " x " + mX + " y " + mY);
    }

    private void touch_move(float x, float y) {
        float dx = Math.abs(x - mX);
        float dy = Math.abs(y - mY);
        if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
            mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
            mX = x;
            mY = y;
            //recordMinMax();
        }
    }

    private void touch_up() {
        mPath.lineTo(mX, mY);
        //recordMinMax();
        mType = TYPE.END;
        // commit the path to our offscreen
        //mCanvas.drawPath(mPath, mPaint);
        //mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SCREEN));
        // kill this so we don't double draw
        //mPath.reset();
        // mPath= new Path();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(mType == TYPE.END)
            return false;

        float x = event.getX();
        float y = event.getY();
        //Log.e("CROP RECT", " l " + Edge.LEFT.getCoordinate() + " r " + Edge.RIGHT.getCoordinate());
        //Log.e("CROP RECT", " t " + Edge.TOP.getCoordinate() + " b " + Edge.BOTTOM.getCoordinate());
        /*int offset = 20;
        if(x < Edge.LEFT.getCoordinate() + offset || x > Edge.RIGHT.getCoordinate() - offset ||
                y < Edge.TOP.getCoordinate() + offset || y > Edge.BOTTOM.getCoordinate() - offset)
            return false;*/

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                touch_start(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                touch_move(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
                touch_up();
                invalidate();
                break;
        }
        return true;
    }

    private void recordMinMax() {
        //if (mType == TYPE.ITEM) {
            if (minXY.x > mX)
                minXY.x = mX;
            if (minXY.y > mY)
                minXY.y = mY;
            if (maxXY.x < mX)
                maxXY.x = mX;
            if (maxXY.y < mY)
                maxXY.y = mY;
        //}
    }

    public void clearAll() {
        for (int i = 0; i < TYPE.END.ordinal(); i++) {
            paths[i] = new Path();
        }
        mType = TYPE.END;
        setupType();
        invalidate();
    }
}
