package com.abggcv.imgproapp;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ImageView;

import com.edmodo.cropper.CropImageView;
import com.edmodo.cropper.cropwindow.edge.Edge;
import com.edmodo.cropper.cropwindow.handle.Handle;
import com.edmodo.cropper.util.AspectRatioUtil;
import com.edmodo.cropper.util.HandleUtil;
import com.edmodo.cropper.util.PaintUtil;

public class MyCropImageView extends ImageView {
    private static final String TAG = CropImageView.class.getName();
    public static final int GUIDELINES_OFF = 0;
    public static final int GUIDELINES_ON_TOUCH = 1;
    public static final int GUIDELINES_ON = 2;
    private Paint mBorderPaint;
    private Paint mGuidelinePaint;
    private Paint mCornerPaint;
    private Paint mSurroundingAreaOverlayPaint;
    private float mHandleRadius;
    private float mSnapRadius;
    private float mCornerThickness;
    private float mBorderThickness;
    private float mCornerLength;
    @NonNull
    private RectF mBitmapRect = new RectF();
    @NonNull
    private PointF mTouchOffset = new PointF();
    private Handle mPressedHandle;
    private boolean mFixAspectRatio;
    private int mAspectRatioX = 1;
    private int mAspectRatioY = 1;
    private int mGuidelinesMode = 1;

    private boolean enableCrop = false;

    public MyCropImageView(Context context) {
        super(context);
        this.init(context, (AttributeSet)null);
    }

    public MyCropImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.init(context, attrs);
    }

    public MyCropImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.init(context, attrs);
    }

    public void enableCrop() {
        enableCrop = true;
        this.invalidate();
    }

    public void disableCrop() {
        enableCrop = false;
        this.invalidate();
    }

    private void init(@NonNull Context context, @Nullable AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, com.edmodo.cropper.R.styleable.CropImageView, 0, 0);
        this.mGuidelinesMode = typedArray.getInteger(com.edmodo.cropper.R.styleable.CropImageView_guidelines, 1);
        this.mFixAspectRatio = typedArray.getBoolean(com.edmodo.cropper.R.styleable.CropImageView_fixAspectRatio, false);
        this.mAspectRatioX = typedArray.getInteger(com.edmodo.cropper.R.styleable.CropImageView_aspectRatioX, 1);
        this.mAspectRatioY = typedArray.getInteger(com.edmodo.cropper.R.styleable.CropImageView_aspectRatioY, 1);
        typedArray.recycle();
        Resources resources = context.getResources();
        this.mBorderPaint = PaintUtil.newBorderPaint(resources);
        this.mGuidelinePaint = PaintUtil.newGuidelinePaint(resources);
        this.mSurroundingAreaOverlayPaint = PaintUtil.newSurroundingAreaOverlayPaint(resources);
        this.mCornerPaint = PaintUtil.newCornerPaint(resources);
        this.mHandleRadius = resources.getDimension(com.edmodo.cropper.R.dimen.target_radius);
        this.mSnapRadius = resources.getDimension(com.edmodo.cropper.R.dimen.snap_radius);
        this.mBorderThickness = resources.getDimension(com.edmodo.cropper.R.dimen.border_thickness);
        this.mCornerThickness = resources.getDimension(com.edmodo.cropper.R.dimen.corner_thickness);
        this.mCornerLength = resources.getDimension(com.edmodo.cropper.R.dimen.corner_length);
    }

    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        this.mBitmapRect = this.getBitmapRect();
        this.initCropWindow(this.mBitmapRect);
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if(!enableCrop)
            return;
        this.drawDarkenedSurroundingArea(canvas);
        this.drawGuidelines(canvas);
        this.drawBorder(canvas);
        this.drawCorners(canvas);
    }

    public boolean onTouchEvent(MotionEvent event) {
        if(!this.isEnabled() || !enableCrop) {
            return false;
        } else {
            switch(event.getAction()) {
                case 0:
                    this.onActionDown(event.getX(), event.getY());
                    return true;
                case 1:
                case 3:
                    this.getParent().requestDisallowInterceptTouchEvent(false);
                    this.onActionUp();
                    return true;
                case 2:
                    this.onActionMove(event.getX(), event.getY());
                    this.getParent().requestDisallowInterceptTouchEvent(true);
                    return true;
                default:
                    return false;
            }
        }
    }

    public void setGuidelines(int guidelinesMode) {
        this.mGuidelinesMode = guidelinesMode;
        this.invalidate();
    }

    public void setFixedAspectRatio(boolean fixAspectRatio) {
        this.mFixAspectRatio = fixAspectRatio;
        this.requestLayout();
    }

    public void setAspectRatio(int aspectRatioX, int aspectRatioY) {
        if(aspectRatioX > 0 && aspectRatioY > 0) {
            this.mAspectRatioX = aspectRatioX;
            this.mAspectRatioY = aspectRatioY;
            if(this.mFixAspectRatio) {
                this.requestLayout();
            }

        } else {
            throw new IllegalArgumentException("Cannot set aspect ratio value to a number less than or equal to 0.");
        }
    }

    public Bitmap getCroppedImage() {
        Drawable drawable = this.getDrawable();
        if(drawable != null && drawable instanceof BitmapDrawable) {
            float[] matrixValues = new float[9];
            this.getImageMatrix().getValues(matrixValues);
            float scaleX = matrixValues[0];
            float scaleY = matrixValues[4];
            float transX = matrixValues[2];
            float transY = matrixValues[5];
            float bitmapLeft = transX < 0.0F?Math.abs(transX):0.0F;
            float bitmapTop = transY < 0.0F?Math.abs(transY):0.0F;
            Bitmap originalBitmap = ((BitmapDrawable)drawable).getBitmap();
            float cropX = (bitmapLeft + Edge.LEFT.getCoordinate()) / scaleX;
            float cropY = (bitmapTop + Edge.TOP.getCoordinate()) / scaleY;
            float cropWidth = Math.min(Edge.getWidth() / scaleX, (float)originalBitmap.getWidth() - cropX);
            float cropHeight = Math.min(Edge.getHeight() / scaleY, (float)originalBitmap.getHeight() - cropY);
            return Bitmap.createBitmap(originalBitmap, (int)cropX, (int)cropY, (int)cropWidth, (int)cropHeight);
        } else {
            return null;
        }
    }

    private RectF getBitmapRect() {
        Drawable drawable = this.getDrawable();
        if(drawable == null) {
            return new RectF();
        } else {
            float[] matrixValues = new float[9];
            this.getImageMatrix().getValues(matrixValues);
            float scaleX = matrixValues[0];
            float scaleY = matrixValues[4];
            float transX = matrixValues[2];
            float transY = matrixValues[5];
            int drawableIntrinsicWidth = drawable.getIntrinsicWidth();
            int drawableIntrinsicHeight = drawable.getIntrinsicHeight();
            int drawableDisplayWidth = Math.round((float)drawableIntrinsicWidth * scaleX);
            int drawableDisplayHeight = Math.round((float)drawableIntrinsicHeight * scaleY);
            float left = Math.max(transX, 0.0F);
            float top = Math.max(transY, 0.0F);
            float right = Math.min(left + (float)drawableDisplayWidth, (float)this.getWidth());
            float bottom = Math.min(top + (float)drawableDisplayHeight, (float)this.getHeight());
            return new RectF(left, top, right, bottom);
        }
    }

    private void initCropWindow(@NonNull RectF bitmapRect) {
        if(this.mFixAspectRatio) {
            this.initCropWindowWithFixedAspectRatio(bitmapRect);
        } else {
            float horizontalPadding = 0.1F * bitmapRect.width();
            float verticalPadding = 0.1F * bitmapRect.height();
            Edge.LEFT.setCoordinate(bitmapRect.left + horizontalPadding);
            Edge.TOP.setCoordinate(bitmapRect.top + verticalPadding);
            Edge.RIGHT.setCoordinate(bitmapRect.right - horizontalPadding);
            Edge.BOTTOM.setCoordinate(bitmapRect.bottom - verticalPadding);
        }

    }

    private void initCropWindowWithFixedAspectRatio(@NonNull RectF bitmapRect) {
        float cropHeight;
        if(AspectRatioUtil.calculateAspectRatio(bitmapRect) > this.getTargetAspectRatio()) {
            cropHeight = AspectRatioUtil.calculateWidth(bitmapRect.height(), this.getTargetAspectRatio());
            Edge.LEFT.setCoordinate(bitmapRect.centerX() - cropHeight / 2.0F);
            Edge.TOP.setCoordinate(bitmapRect.top);
            Edge.RIGHT.setCoordinate(bitmapRect.centerX() + cropHeight / 2.0F);
            Edge.BOTTOM.setCoordinate(bitmapRect.bottom);
        } else {
            cropHeight = AspectRatioUtil.calculateHeight(bitmapRect.width(), this.getTargetAspectRatio());
            Edge.LEFT.setCoordinate(bitmapRect.left);
            Edge.TOP.setCoordinate(bitmapRect.centerY() - cropHeight / 2.0F);
            Edge.RIGHT.setCoordinate(bitmapRect.right);
            Edge.BOTTOM.setCoordinate(bitmapRect.centerY() + cropHeight / 2.0F);
        }

    }

    private void drawDarkenedSurroundingArea(@NonNull Canvas canvas) {
        RectF bitmapRect = this.mBitmapRect;
        float left = Edge.LEFT.getCoordinate();
        float top = Edge.TOP.getCoordinate();
        float right = Edge.RIGHT.getCoordinate();
        float bottom = Edge.BOTTOM.getCoordinate();
        canvas.drawRect(bitmapRect.left, bitmapRect.top, bitmapRect.right, top, this.mSurroundingAreaOverlayPaint);
        canvas.drawRect(bitmapRect.left, bottom, bitmapRect.right, bitmapRect.bottom, this.mSurroundingAreaOverlayPaint);
        canvas.drawRect(bitmapRect.left, top, left, bottom, this.mSurroundingAreaOverlayPaint);
        canvas.drawRect(right, top, bitmapRect.right, bottom, this.mSurroundingAreaOverlayPaint);
    }

    private void drawGuidelines(@NonNull Canvas canvas) {
        if(this.shouldGuidelinesBeShown()) {
            float left = Edge.LEFT.getCoordinate();
            float top = Edge.TOP.getCoordinate();
            float right = Edge.RIGHT.getCoordinate();
            float bottom = Edge.BOTTOM.getCoordinate();
            float oneThirdCropWidth = Edge.getWidth() / 3.0F;
            float x1 = left + oneThirdCropWidth;
            canvas.drawLine(x1, top, x1, bottom, this.mGuidelinePaint);
            float x2 = right - oneThirdCropWidth;
            canvas.drawLine(x2, top, x2, bottom, this.mGuidelinePaint);
            float oneThirdCropHeight = Edge.getHeight() / 3.0F;
            float y1 = top + oneThirdCropHeight;
            canvas.drawLine(left, y1, right, y1, this.mGuidelinePaint);
            float y2 = bottom - oneThirdCropHeight;
            canvas.drawLine(left, y2, right, y2, this.mGuidelinePaint);
        }
    }

    private void drawBorder(@NonNull Canvas canvas) {
        canvas.drawRect(Edge.LEFT.getCoordinate(), Edge.TOP.getCoordinate(), Edge.RIGHT.getCoordinate(), Edge.BOTTOM.getCoordinate(), this.mBorderPaint);
    }

    private void drawCorners(@NonNull Canvas canvas) {
        float left = Edge.LEFT.getCoordinate();
        float top = Edge.TOP.getCoordinate();
        float right = Edge.RIGHT.getCoordinate();
        float bottom = Edge.BOTTOM.getCoordinate();
        float lateralOffset = (this.mCornerThickness - this.mBorderThickness) / 2.0F;
        float startOffset = this.mCornerThickness - this.mBorderThickness / 2.0F;
        canvas.drawLine(left - lateralOffset, top - startOffset, left - lateralOffset, top + this.mCornerLength, this.mCornerPaint);
        canvas.drawLine(left - startOffset, top - lateralOffset, left + this.mCornerLength, top - lateralOffset, this.mCornerPaint);
        canvas.drawLine(right + lateralOffset, top - startOffset, right + lateralOffset, top + this.mCornerLength, this.mCornerPaint);
        canvas.drawLine(right + startOffset, top - lateralOffset, right - this.mCornerLength, top - lateralOffset, this.mCornerPaint);
        canvas.drawLine(left - lateralOffset, bottom + startOffset, left - lateralOffset, bottom - this.mCornerLength, this.mCornerPaint);
        canvas.drawLine(left - startOffset, bottom + lateralOffset, left + this.mCornerLength, bottom + lateralOffset, this.mCornerPaint);
        canvas.drawLine(right + lateralOffset, bottom + startOffset, right + lateralOffset, bottom - this.mCornerLength, this.mCornerPaint);
        canvas.drawLine(right + startOffset, bottom + lateralOffset, right - this.mCornerLength, bottom + lateralOffset, this.mCornerPaint);
    }

    private boolean shouldGuidelinesBeShown() {
        return this.mGuidelinesMode == 2 || this.mGuidelinesMode == 1 && this.mPressedHandle != null;
    }

    private float getTargetAspectRatio() {
        return (float)this.mAspectRatioX / (float)this.mAspectRatioY;
    }

    private void onActionDown(float x, float y) {
        float left = Edge.LEFT.getCoordinate();
        float top = Edge.TOP.getCoordinate();
        float right = Edge.RIGHT.getCoordinate();
        float bottom = Edge.BOTTOM.getCoordinate();
        this.mPressedHandle = HandleUtil.getPressedHandle(x, y, left, top, right, bottom, this.mHandleRadius);
        if(this.mPressedHandle != null) {
            HandleUtil.getOffset(this.mPressedHandle, x, y, left, top, right, bottom, this.mTouchOffset);
            this.invalidate();
        }

    }

    private void onActionUp() {
        if(this.mPressedHandle != null) {
            this.mPressedHandle = null;
            this.invalidate();
        }

    }

    private void onActionMove(float x, float y) {
        if(this.mPressedHandle != null) {
            x += this.mTouchOffset.x;
            y += this.mTouchOffset.y;
            if(this.mFixAspectRatio) {
                this.mPressedHandle.updateCropWindow(x, y, this.getTargetAspectRatio(), this.mBitmapRect, this.mSnapRadius);
            } else {
                this.mPressedHandle.updateCropWindow(x, y, this.mBitmapRect, this.mSnapRadius);
            }

            this.invalidate();
        }
    }
}


