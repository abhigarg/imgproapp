package com.abggcv.imgproapp;

/**
 * Created by ABGG on 13/12/2015.
 */

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SubMenu;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

//import android.graphics.Rect;


@SuppressWarnings("deprecation")
public class MainActivityOld extends AppCompatActivity implements OnTouchListener{

    private String TAG = "DrawActivity";
    ImageView imageResult, imageDrawingPane;
    Bitmap bitmapMaster, bitmapOverlay;
    Canvas canvasMaster, canvasOverlay;

    Paint paintItem, paintBack, paintRect, temPaint;
    int strokeWidth = 3;
    Path pathItem, pathBack;

    float downx = 0, downy = 0, upx = 0, upy = 0;

    private float mX, mY;
    private static final float TOUCH_TOLERANCE = 1;
    private static final double RECT_TOLERANCE = 10;

    private List<Point> ptsItem, ptsBack;
    private Point minXY, maxXY;

    //Menu
    private MenuItem[] mMenuItems;
    private SubMenu mSubMenu;

    private MenuItem[] mProcessingMenuItems;
    private SubMenu mProcessingSubMenu;

    boolean selectItem, selectBackground, drawRect, moveRect, selTL, selTR, selBL, selBR;

    private String extStorageDirectory = Environment.getExternalStorageDirectory().getPath();

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i(TAG, "OpenCV loaded successfully");
                    // Load native library after(!) OpenCV initialization
                    System.loadLibrary("image_processing");

                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_touch_draw);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_bottom);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        imageResult = (ImageView)findViewById(R.id.result);
        imageDrawingPane = (ImageView)findViewById(R.id.drawingpane);

        imageDrawingPane.setOnTouchListener(this);

        selectItem = false;
        paintItem = new Paint();
        paintItem.setStyle(Paint.Style.STROKE);
        paintItem.setColor(Color.GREEN);
        paintItem.setStrokeWidth(strokeWidth);

        selectBackground = false;
        paintBack = new Paint();
        paintBack.setStyle(Paint.Style.STROKE);
        paintBack.setColor(Color.RED);
        paintBack.setStrokeWidth(strokeWidth);

        drawRect = true;
        paintRect = new Paint();
        paintRect.setStyle(Paint.Style.STROKE);
        paintRect.setColor(Color.BLUE);
        paintRect.setStrokeWidth(strokeWidth);

        temPaint = new Paint();
        temPaint.setStyle(Paint.Style.STROKE);
        temPaint.setColor(Color.MAGENTA);
        temPaint.setStrokeWidth(strokeWidth);

        moveRect = false;

        pathItem = new Path();
        pathBack = new Path();

    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onResume(){
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_11, this, mLoaderCallback);
        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);

        /*Log.e(TAG, "Menu Created");
        mSubMenu = menu.addSubMenu("Choose");
        mProcessingSubMenu = menu.addSubMenu("Processing");

        mMenuItems = new MenuItem[6];
        mMenuItems[0] = mSubMenu.add(1, 0, Menu.NONE, "Choose Picture");
        mMenuItems[1] = mSubMenu.add(1, 1, Menu.NONE, "Clear Drawing");
        mMenuItems[2] = mSubMenu.add(1, 2, Menu.NONE, "Choose Stroke Width");
        mMenuItems[3] = mSubMenu.add(1, 3, Menu.NONE, "Draw Rectangle");
        mMenuItems[4] = mSubMenu.add(1, 4, Menu.NONE, "Select Crop Item");
        mMenuItems[5] = mSubMenu.add(1, 5, Menu.NONE, "Select Background");

        mProcessingMenuItems = new MenuItem[1];
        mProcessingMenuItems[0] = mProcessingSubMenu.add(2, 0, Menu.NONE, "Crop Item");*/

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Log.i(TAG, "Menu option selected");

        //int id = item.getItemId();
        //if (item.getGroupId() == R.id.action_choose) {

            if(item.getItemId() == R.id.action_choose_picture){

                Log.i(TAG, " Selected choose picture");

                Intent choosePictureIntent = new Intent(Intent.ACTION_PICK,
                                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(choosePictureIntent, 1);
            }

            if(item.getItemId() == R.id.action_clear){

                Log.i(TAG, " Selected clear drawing");

                //startActivityForResult(choosePictureIntent, 1);

                if(!pathItem.isEmpty()) {
                    pathItem.reset();
                    //paintItem.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
                    ptsItem.clear();
                }

                if(!ptsBack.isEmpty()) {
                    pathBack.reset();
                    //paintBack.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
                    ptsBack.clear();
                }

                minXY.x = bitmapMaster.getWidth();
                minXY.y = bitmapMaster.getHeight();

                maxXY.x = 0;
                maxXY.y = 0;

                imageDrawingPane.setImageBitmap(null);
                bitmapOverlay.recycle();

                bitmapOverlay = Bitmap.createBitmap(bitmapMaster.getWidth(), bitmapMaster.getHeight(), bitmapMaster.getConfig());
                canvasOverlay= new Canvas(bitmapOverlay);

                imageDrawingPane.setImageBitmap(bitmapOverlay);
                imageDrawingPane.invalidate();

            }

            if(item.getItemId() == R.id.action_choose_sw)
                showInputDialogStrokeWidth();

            if(item.getItemId() == R.id.action_draw_rect){ //Draw rectangle
                drawRect = true;
                selectBackground = false;
                selectItem = false;
                moveRect = false;
                paintRect.setStrokeWidth(strokeWidth);
            }

            if(item.getItemId() == R.id.action_select_crop_item) { //select foreground
                drawRect = false;
                moveRect = false;
                selectBackground = false;
                selectItem = true;
                paintItem.setStrokeWidth(strokeWidth);

            }

            if(item.getItemId() == R.id.action_select_bk) { //select background
                drawRect = false;
                moveRect = false;
                selectBackground = true;
                selectItem = false;
                paintBack.setStrokeWidth(strokeWidth);
            }

        //}


            //apply grabcut
            if(item.getItemId() == R.id.action_apply) {

                Mat img = new Mat();

                //Bitmap bmp32 = bitmapMaster.copy(Bitmap.Config.ARGB_8888, true);
                if(bitmapMaster == null) {
                    Toast.makeText(this, "Bitmap NULL", Toast.LENGTH_SHORT).show();
                    return true;
                }
                Utils.bitmapToMat(bitmapMaster, img);

                Log.d(TAG, "Size of image (numRows, numCols) " + img.rows() + "x" + img.cols());

                if (ptsItem.isEmpty())
                    Toast.makeText(this, "No object/background points selected. Please touch to select some object points", Toast.LENGTH_SHORT).show();
                else {

                    Log.d(TAG, "Number of foreground points: " + ptsItem.size());
                    Log.d(TAG, "Size of bitmap (Height, Width): " + bitmapMaster.getHeight() + "x" + bitmapMaster.getWidth());

                    Mat mask = new Mat((int) (img.rows() / 5), (int) (img.cols() / 5), CvType.CV_8UC1, new Scalar(0)); //set to BGD

                    Log.d(TAG, "Size of mask(numRows, numCols): " + mask.rows() + "x" + mask.cols());

                    //set rectangular area to probable foreground
                    int rectX = (int) Math.max(0, minXY.x / 5);
                    int rectY = (int) Math.max(0, minXY.y / 5);

                    int rectW = (int) Math.min(maxXY.x / 5 - minXY.x / 5, img.cols() - rectX);
                    int rectH = (int) Math.min(maxXY.y / 5 - minXY.y / 5, img.rows() - rectY);
                    Rect rect = new Rect(rectX, rectY, rectW, rectH);

                    for (int c = rectX; c < rectX + rectW; c++)
                        for (int r = rectY; r < rectY + rectH; r++)
                            mask.put(r, c, (byte) (3 & 0xFF)); //probable foreground


                    for (int i = 0; i < ptsItem.size(); i++) { //sure foreground
                        mask.put((int) ptsItem.get(i).y, (int) ptsItem.get(i).x, (byte) (1 & 0xFF));
                    }

                    for (int i = 0; i < ptsBack.size(); i++) { //sure background
                        mask.put((int) ptsBack.get(i).y, (int) ptsBack.get(i).x, (byte) (0 & 0xFF));
                    }

                    //applyGrabCut(grabcut_img, mask);
                    String imgName = extStorageDirectory + "/input.jpg";
                    String outImgName = extStorageDirectory + "/output.jpg";
                    Log.d(TAG, "output path: " + outImgName);
                    ApplyGrabcut(img.getNativeObjAddr(), mask.getNativeObjAddr(), imgName, outImgName,
                            (int) minXY.x, (int) minXY.y, (int) maxXY.x, (int) maxXY.y);

                    Toast.makeText(this, "Item cropping completed", Toast.LENGTH_SHORT).show();

                    File file = new File(outImgName);

                    if (file.exists()) {

                        int reqHeight = imageResult.getHeight();
                        int reqWidth = imageResult.getWidth();

                        if (bitmapMaster != null) {
                            imageResult.setImageBitmap(null);
                            bitmapMaster.recycle();
                            imageDrawingPane.setImageBitmap(null);
                            bitmapOverlay.recycle();
                        }

                        BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
                        bmpFactoryOptions.inJustDecodeBounds = true;
                        bmpFactoryOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;
                        Bitmap tempBitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), bmpFactoryOptions);

                        int height = bmpFactoryOptions.outHeight;
                        int width = bmpFactoryOptions.outWidth;

                        int inSampleSize = 1;

                        if (height > reqHeight)
                            inSampleSize = Math.round((float) height / (float) reqHeight);

                        int expectedWidth = width / inSampleSize;

                        if (expectedWidth > reqWidth)
                            inSampleSize = Math.round((float) width / (float) reqWidth);

                        bmpFactoryOptions.inSampleSize = inSampleSize;

                        bmpFactoryOptions.inJustDecodeBounds = false;
                        tempBitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), bmpFactoryOptions);

                        Bitmap.Config config;
                        if (tempBitmap.getConfig() != null) {
                            config = tempBitmap.getConfig();
                        } else {
                            config = Bitmap.Config.ARGB_8888;
                        }

                        bitmapMaster = Bitmap.createBitmap(tempBitmap.getWidth(), tempBitmap.getHeight(), config);
                        canvasMaster = new Canvas(bitmapMaster);
                        canvasMaster.drawBitmap(tempBitmap, new Matrix(), null);
                        imageResult.setImageBitmap(bitmapMaster);

                        bitmapOverlay = Bitmap.createBitmap(tempBitmap.getWidth(), tempBitmap.getHeight(), config);
                        canvasOverlay = new Canvas(bitmapOverlay);
                        imageDrawingPane.setImageBitmap(bitmapOverlay);

                    }
                }
            }

        return true;

    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Bitmap tempBitmap;

        if (requestCode == 1 && resultCode == RESULT_OK && null!= data) {
            Uri imageFileUri = data.getData();
            //Picasso.with(this).load(imageFileUri).into(imageResult);

            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            try {
                BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
                bmpFactoryOptions.inJustDecodeBounds = true;

                bmpFactoryOptions.inJustDecodeBounds = false;
                tempBitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(
                        imageFileUri), null, bmpFactoryOptions);

                Bitmap.Config config;
                if(tempBitmap.getConfig() != null){
                    config = tempBitmap.getConfig();
                }else{
                    config = Bitmap.Config.ARGB_8888;
                }

                if(bitmapMaster!=null) {
                    imageResult.setImageBitmap(null);
                    bitmapMaster.recycle();
                    imageDrawingPane.setImageBitmap(null);
                    bitmapOverlay.recycle();
                }
                //bitmapMaster is Mutable bitmap
                bitmapMaster = Bitmap.createBitmap(tempBitmap.getWidth(), tempBitmap.getHeight(), config);
                bitmapOverlay = Bitmap.createBitmap(tempBitmap.getWidth(), tempBitmap.getHeight(), config);
                //bitmapOverlayRect = Bitmap.createBitmap(tempBitmap.getWidth(), tempBitmap.getHeight(), config);
                Matrix matrix = new Matrix();

                ptsItem = new ArrayList<Point>();
                ptsBack = new ArrayList<Point>();

                canvasMaster = new Canvas(bitmapMaster);
                canvasMaster.drawBitmap(tempBitmap, matrix, null);
                imageResult.setImageBitmap(bitmapMaster);

                canvasOverlay= new Canvas(bitmapOverlay);
                imageDrawingPane.setImageBitmap(bitmapOverlay);

                minXY = new Point();
                maxXY = new Point();

                minXY.x = tempBitmap.getWidth();
                minXY.y = tempBitmap.getHeight();

                maxXY.x = 0;
                maxXY.y = 0;

            } catch (Exception e) {
                Log.v("ERROR", e.toString());
            }
        }
    }


    private void touch_start(ImageView iv, Bitmap bm, float x, float y) {
        //mPath.reset();
        if(bitmapMaster != null && bitmapOverlay == null) {
            Log.i(TAG, "Re-creating bitmapOverlay");
            bitmapOverlay = Bitmap.createBitmap(bitmapMaster.getWidth(), bitmapMaster.getHeight(), bitmapMaster.getConfig());
            canvasOverlay= new Canvas(bitmapOverlay);
            canvasMaster.drawBitmap(bitmapOverlay, new Matrix(), null);
            imageDrawingPane.setImageBitmap(bitmapOverlay);
            imageDrawingPane.invalidate();
        }
        else if(bitmapMaster == null)
            Toast.makeText(this, "Load an image first", Toast.LENGTH_SHORT).show();

        else {

            if(selectItem)
                pathItem = new Path();
            if(selectBackground)
                pathBack = new Path();

            float projectedX = x * (float) bm.getWidth() / (float) iv.getWidth();
            float projectedY = y * (float) bm.getHeight() / (float) iv.getHeight();

            //pts.add(new Point(projectedX, projectedY));
            if(selectItem)
                pathItem.moveTo(projectedX, projectedY);
            if(selectBackground)
                pathBack.moveTo(projectedX, projectedY);

            mX = projectedX;
            mY = projectedY;

            if(selectItem || drawRect) {
                if(minXY.x > mX)
                    minXY.x = mX;
                if(minXY.y > mY)
                    minXY.y = mY;
                if(maxXY.x < mX)
                    maxXY.x = mX;
                if(maxXY.y < mY)
                    maxXY.y = mY;

            }
        }
    }

    private void rect_move(ImageView iv, Bitmap bm, float x, float y) {

        float projectedX = x *(float)bm.getWidth()/(float)iv.getWidth();
        float projectedY = y *(float)bm.getHeight()/(float)iv.getHeight();

        float dx = Math.abs(projectedX - mX);
        float dy = Math.abs(projectedY - mY);


        if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {

            mX = projectedX;
            mY = projectedY;

            //canvasOverlay.drawRect((float) minXY.x, (float) minXY.y, (float) maxXY.x, (float) maxXY.y, paintRemove);

            maxXY.x = projectedX;
            maxXY.y = projectedY;


            //canvasOverlay.drawRect((float) minXY.x, (float) minXY.y, (float) maxXY.x, (float) maxXY.y, paintRect);

        }

    }

    private void touch_move(ImageView iv, Bitmap bm, float x, float y) {

        float projectedX = x *(float)bm.getWidth()/(float)iv.getWidth();
        float projectedY = y *(float)bm.getHeight()/(float)iv.getHeight();

        float dx = Math.abs(projectedX - mX);
        float dy = Math.abs(projectedY - mY);


        if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
            if(selectItem)
                pathItem.quadTo(mX, mY, projectedX, projectedY);

            if(selectBackground)
                pathBack.quadTo(mX, mY, projectedX, projectedY);

            mX = projectedX;
            mY = projectedY;

            //pts.add(new Point(mX, mY));

            if(selectItem) {
                if (minXY.x > mX)
                    minXY.x = mX;

                if (minXY.y > mY)
                    minXY.y = mY;

                if (maxXY.x < mX)
                    maxXY.x = mX;

                if (maxXY.y < mY)
                    maxXY.y = mY;
            }
        }
    }

    private void touch_up() {

        if (selectItem) {
            pathItem.lineTo(mX, mY);
            // commit the path to our offscreen
            canvasOverlay.drawPath(pathItem, paintItem);

            if (minXY.x > mX)
                minXY.x = mX;

            if (minXY.y > mY)
                minXY.y = mY;

            if (maxXY.x < mX)
                maxXY.x = mX;

            if (maxXY.y < mY)
                maxXY.y = mY;

            //add points from path to arrayList of opencv points
            getItemPoints();
        }

        if (selectBackground) {
            pathBack.lineTo(mX, mY);
            // commit the path to our offscreen
            canvasOverlay.drawPath(pathBack, paintBack);

            //add points from path to arrayList of opencv points
            getBackPoints();
        }

        if(drawRect) {
            if (minXY.x > mX)
                minXY.x = mX;

            if (minXY.y > mY)
                minXY.y = mY;

            if (maxXY.x < mX)
                maxXY.x = mX;

            if (maxXY.y < mY)
                maxXY.y = mY;
            canvasOverlay.drawRect((float) minXY.x, (float) minXY.y, (float) maxXY.x, (float) maxXY.y, paintRect);
            moveRect = true;
        }
    }


    //@Override
    public boolean onTouch(View v, MotionEvent event) {
        float x = event.getX();
        float y = event.getY();

        ImageView iv = (ImageView) v;

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if(!moveRect) {
                    touch_start(iv, bitmapOverlay, x, y);
                    imageDrawingPane.invalidate();
                }

                else{
                    selTL = false;
                    selTR = false;
                    selBR = false;
                    selBL = false;

                    mX = x * (float) bitmapOverlay.getWidth() / (float) iv.getWidth();
                    mY = y * (float) bitmapOverlay.getHeight() / (float) iv.getHeight();

                    double dtl = (minXY.x - mX)*(minXY.x - mX) + (minXY.y - mY)*(minXY.y - mY);
                    double dtr = (maxXY.x - mX)*(maxXY.x - mX) + (minXY.y - mY)*(minXY.y - mY);
                    double dbl = (minXY.x - mX)*(minXY.x - mX) + (maxXY.y - mY)*(maxXY.y - mY);
                    double dbr = (maxXY.x - mX)*(maxXY.x - mX) + (maxXY.y - mY)*(maxXY.y - mY);

                    double dmin = Math.min(Math.min(dtl, dtr), Math.min(dbl, dbr));

                    if(dmin < RECT_TOLERANCE) {

                        if (dmin == dbl) {
                            selBL = true;
                        }
                        if (dmin == dbr) {
                            selBR = true;
                        }
                        if (dmin == dtl) {
                            selTL = true;
                        }
                        if (dmin == dtr) {
                            selTR = true;
                        }
                    }

                }


                break;
            case MotionEvent.ACTION_MOVE:
                if(!drawRect)
                    touch_move(iv, bitmapOverlay, x, y);
                else
                    rect_move(iv, bitmapOverlay, x, y);

                imageDrawingPane.invalidate();

                break;
            case MotionEvent.ACTION_UP:
                if(!moveRect) {
                    touch_up();
                    imageDrawingPane.invalidate();
                }
                else{

                    float projectedX = x * (float) bitmapOverlay.getWidth() / (float) iv.getWidth();
                    float projectedY = y * (float) bitmapOverlay.getHeight() / (float) iv.getHeight();

                    if(selBL){
                        minXY.x = projectedX;
                        maxXY.y = projectedY;
                    }

                    if(selTL){
                        minXY.x = projectedX;
                        minXY.y = projectedY;
                    }

                    if(selBR){
                        maxXY.x = projectedX;
                        maxXY.y = projectedY;
                    }

                    if(selTR){
                        maxXY.x = projectedX;
                        minXY.y = projectedY;
                    }

                    //clear previous rectangle
                    imageDrawingPane.setImageBitmap(null);
                    bitmapOverlay.recycle();

                    bitmapOverlay = Bitmap.createBitmap(bitmapMaster.getWidth(), bitmapMaster.getHeight(), bitmapMaster.getConfig());
                    canvasOverlay = new Canvas(bitmapOverlay);

                    imageDrawingPane.setImageBitmap(bitmapOverlay);
                    imageDrawingPane.invalidate();

                    //new rectangle
                    //canvasOverlay.drawRect((float) minx, (float) miny, (float) maxx, (float) maxy, temPaint);
                    //imageDrawingPane.invalidate();
                    canvasOverlay.drawRect((float) minXY.x, (float) minXY.y, (float) maxXY.x, (float) maxXY.y, paintRect);
                    imageDrawingPane.invalidate();

                    selBL = false; selBR = false; selTL = false; selTR = false;

                }
                break;
        }
        return true;
    }


    //save opencv mat to file
    public void SaveImage (Mat mRgb, String imgName) {
        Mat mIntermediateMat = new Mat();
        Imgproc.cvtColor(mRgb, mIntermediateMat, Imgproc.COLOR_RGB2BGR, 3);

        File path = new File(Environment.getExternalStorageDirectory() + "/");
        //path.mkdirs();
        File file = new File(path, imgName);

        String filename = file.toString();
        Boolean bool = Highgui.imwrite(filename, mIntermediateMat);

        if (bool)
            Log.i(TAG, "SUCCESS writing image to external storage");
        else
            Log.i(TAG, "Fail writing image to external storage");
    }


    //Input for stroke width
    protected void showInputDialogStrokeWidth() {
        // get prompts.xml view
        LayoutInflater layoutInflater = LayoutInflater.from(MainActivityOld.this);
        View promptView = layoutInflater.inflate(R.layout.input_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivityOld.this);
        alertDialogBuilder.setView(promptView);

        final EditText editText = (EditText) promptView.findViewById(R.id.edittext);
        // setup a dialog window
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //resultText.setText("Hello, " + editText.getText());
                        Toast.makeText(MainActivityOld.this, "Stroke width: " + editText.getText(),Toast.LENGTH_SHORT).show();
                        strokeWidth = Integer.parseInt(editText.getText().toString()); //editText.getText();
                    }
                })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }


    //Item points to opencv array of points
    public void getItemPoints() {
        PathMeasure pm = new PathMeasure(pathItem, false);
        float length = pm.getLength();
        float distance = 0f;
        float speed = length / 20;
        int counter = 0;
        float[] aCoordinates = new float[2];

        while ((distance < length) && (counter < 20)) {
            // get point from the path
            pm.getPosTan(distance, aCoordinates, null);
            ptsItem.add(new Point(aCoordinates[0]/5, aCoordinates[1]/5));
            counter++;
            distance = distance + speed;
        }
    }

    //Item points to opencv array of points
    public void getBackPoints() {
        PathMeasure pm = new PathMeasure(pathBack, false);
        float length = pm.getLength();
        float distance = 0f;
        float speed = length / 20;
        int counter = 0;
        float[] aCoordinates = new float[2];

        while ((distance < length) && (counter < 20)) {
            // get point from the path
            pm.getPosTan(distance, aCoordinates, null);
            ptsBack.add(new Point(aCoordinates[0]/5, aCoordinates[1]/5));
            counter++;
            distance = distance + speed;
        }
    }

    //jni functions declaration
    @SuppressWarnings("JniMissingFunction")
    public native int ApplyGrabcut(long imgAdd, long maskAdd, String filePath, String outputPath, int minX, int minY, int maxX, int maxY);

}
