package com.imagepro;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.widget.ImageView;

import com.edmodo.cropper.CropImageView;

import java.lang.ref.WeakReference;

/**
 * Created by shakti on 1/10/2016.
 */
public class ImageCropTask  extends AsyncTask<Integer, Void, Bitmap> {
    private final WeakReference<CropImageView> from;
    private final WeakReference<ImageView> to;

    private int data = 0;

    public ImageCropTask(CropImageView f, ImageView t) {
        // Use a WeakReference to ensure the ImageView can be garbage collected
        from = new WeakReference<CropImageView>(f);
        to = new WeakReference<ImageView>(t);
    }

    // Decode image in background.
    @Override
    protected Bitmap doInBackground(Integer... params) {
        data = params[0];
        final CropImageView cropImageView = from.get();
        return cropImageView.getCroppedImage();
    }

    // Once complete, see if ImageView is still around and set bitmap.
   /* @Override
    protected void onPostExecute(Bitmap bitmap) {
        if (imageViewReference != null && bitmap != null) {
            final ImageView imageView = imageViewReference.get();
            if (imageView != null) {
                imageView.setImageBitmap(bitmap);
            }
        }
    }*/
}
