#include <jni.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <vector>
#include <android/log.h>

#include <sstream>
#include <iostream>
#include <string>
#include <stdio.h>



#define LOG_TAG "Jni_Part"
#define LOGI(...) __android_log_print(ANDROID_LOG_INFO, LOG_TAG,__VA_ARGS__)
#define LOGD(...) ((void)__android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__))

using namespace std;
using namespace cv;

extern "C"
{
//for applying grabcut: com.abggcv.imgproapp
// outputType
// 0 -- black-n-white and colored object output
// 1 -- identified mask for object
// 2 -- input mask is outputted
JNIEXPORT jint JNICALL Java_com_abggcv_imgproapp_MainActivity_ApplyGrabcut(JNIEnv *env, jobject, jlong addrRgba, jlong addrMask, jlong addrMaskOut);

JNIEXPORT jint JNICALL Java_com_abggcv_imgproapp_MainActivity_ApplyFilter(JNIEnv *env, jobject, jlong addrRgba, jlong addrMask, jlong addrResult,
                                                                          int minX, int minY, int maxX, int maxY);

JNIEXPORT jint JNICALL Java_com_abggcv_imgproapp_MainActivity_ApplyGrabcut(JNIEnv *env, jobject, jlong addrRgba, jlong addrMask, jlong addrMaskOut)
{
    LOGD("jni starts");

    Mat &image = *(Mat *) addrRgba;  //input image

    Mat &mask = *(Mat *) addrMask;

    Mat &maskOut = *(Mat *)addrMaskOut;

    //Mat &bgModel = *(Mat *)addrBgModel;

    //Mat &fgModel = *(Mat *)addrFgModel;

    Mat img;
    cvtColor(image, img, CV_RGBA2BGR);

    Mat img_resz;

    resize(img, img_resz, Size(img.cols / 5, img.rows / 5), 0, 0, CV_INTER_AREA);

    Mat bgModel, fgModel;

    Rect rect = Rect(Point(0, 0), Point(img_resz.cols, img_resz.rows));

    //LOGD("size of mask: %d,%d", mask.cols, mask.rows);
    //LOGD("size of imgresz: %d, %d", img_resz.cols, img_resz.rows);

    grabCut(img_resz, mask, rect, bgModel, fgModel, 5, GC_INIT_WITH_MASK);

    /*else if(runType == 1)
        grabCut(img_resz, mask, rect, bgModel, fgModel, 1, GC_INIT_WITH_MASK + GC_INIT_WITH_RECT);
    */    /*else if(runType == 2)
            grabCut(img_resz, mask, rect, bgModel, fgModel, 1, GC_INIT_WITH_RECT);*/

        LOGD("Grabcut applied successfully");

        //compare(mask, GC_PR_FGD, mask, CMP_EQ);

        Mat temp_mask1, temp_mask2;// temp_mask;
        compare(mask, GC_PR_FGD, temp_mask1, CMP_EQ);
        compare(mask, GC_FGD, temp_mask2, CMP_EQ);
        bitwise_or(temp_mask1, temp_mask2, maskOut);

        //compare(mask, GC_FGD, maskOut, CMP_EQ);

        /*//resize mask to full image size
        resize(mask, maskOut, Size(img.cols, img.rows), 0, 0, CV_INTER_AREA);

        threshold(maskOut, maskOut, 0, 255, CV_THRESH_BINARY);
    */

        /*Mat foreground; //(img.size(), CV_8UC3, Scalar(0, 0, 0));

        Mat gray_img;
        cvtColor(img, gray_img, CV_BGR2GRAY);
        cvtColor(gray_img, foreground, CV_GRAY2BGR);

        img.copyTo(foreground, mask_resz);

        *//* rect = Rect(Point((int) (minX), (int) (minY)), Point((int) (maxX), (int) (maxY)));
     rectangle(foreground, rect, Scalar(0, 255, 0), 2);*//*

    LOGI("Writing grabcut output image to file");
    imwrite(stdOutputFileName, foreground);*/

    return 1;

}

JNIEXPORT jint JNICALL Java_com_abggcv_imgproapp_MainActivity_ApplyFilter(JNIEnv *env, jobject, jlong addrInput, jlong addrMask,
                                                                          jlong addrResult, int minX, int minY, int maxX, int maxY){

    LOGD("apply filter");

    Mat &input = *(Mat *) addrInput;  //input image

    Mat &maskIn = *(Mat *) addrMask;

    Mat &result = *(Mat *)addrResult;

    Mat img;
    cvtColor(input, img, CV_RGBA2BGR);

    //resize mask to cropped image size
    Mat maskresz;
    resize(maskIn, maskresz, Size(5*(maxX - minX), 5*(maxY - minY)), 0, 0, CV_INTER_AREA);
    threshold(maskresz, maskresz, 0, 255, CV_THRESH_BINARY);

    Mat mask(img.size(), CV_8UC1, Scalar(0));
    /*Mat tempMask(img.size(), CV_8UC1, Scalar(0));
    rectangle(tempMask, Point(minX, minY), Point(maxX, maxY), Scalar(255),-1);
    maskIn.copyTo(mask, tempMask);*/
    maskresz.copyTo(mask(Rect(Point(5*minX, 5*minY), Point(5*maxX, 5*maxY)))); //Point(minX + maskIn.cols, minY + maskIn.rows)


    LOGD("Mask created");

    //for Black-White result
    /*Mat gray;
    cvtColor(input, gray, CV_RGBA2GRAY);
    cvtColor(gray, result, CV_GRAY2RGBA);

    input.copyTo(result, mask);*/

    //for making translucent highlighted result
    Mat redMat(img.size(), CV_8UC3, Scalar(255, 0, 255));
    Mat blendMat;//(input.size(), CV_8UC3, Scalar(0,0,0));
    input.copyTo(blendMat);
    redMat.copyTo(blendMat, mask);
    double alpha = 0.5;

    Mat resultBgr;
    addWeighted(img, alpha, blendMat, 1-alpha, 0, resultBgr);

    cvtColor(resultBgr, result, CV_BGR2RGBA);

    return 1;
}


}